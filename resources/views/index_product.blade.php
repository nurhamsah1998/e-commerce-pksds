@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Products') }}</div>
                <div class="card-group m-auto">
                    @foreach ($products as $product)
                    <div class="card" style="width: 18rem;">
                        <img src="{{ url('product_img/'. $product->image) }}" class="card-img-top" alt="product">
                        <div class="card-body">
                            <div style="margin-bottom: 15px;">
                                <p class="card-text" style="background-color: green; color:#fff;padding:3px 5px;display:inline; border-radius:5px; margin-bottom:5px;">{{ $product->category->name }}</p></div>
                          <h5 class="card-title">{{$product->name}}</h5>
                          <p class="card-text">{{$product->description}}</p>
                          <a href="/product/{{$product->id}}" class="btn btn-primary">Show detail</a>
                        </div>
                      </div>
                      
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection