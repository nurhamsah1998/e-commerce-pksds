@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('INI ADALAH TAMPILAN UNTUK USER') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('NANTI DISINI BAKALAN ADA DAFTAR PRODUK JUALAN ADMIN DAN SI USER DAPAT MEMBELI JIKA SUDAH LOGIN. KALAU TIDAK LOGIN SI USER BISA AKSES HALAMAN INI TETAPI HANYA BISA MELIHAT DAFTAR PRODUK AJA, TIDAK BISA MEMBELI') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
