@extends('admin.components.drawer')

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Product Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/admin/product" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama Produk</label>
          <input type="text" name="name" id="name" class="form-control" placeholder="Masukan nama produk">
          @error('name')
              <div style="color: red; font-size:19px; margin-left:10px">
                *{{$message}}
              </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Stock</label>
          <input type="number" name="stock" id="stock" class="form-control" placeholder="Masukan stock produk">
          @error('stock')
              <div style="color: red; font-size:19px; margin-left:10px">
                *{{$message}}
              </div>
          @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Harga</label>
            <input type="number" name="price" id="price" class="form-control" placeholder="Masukan harga produk">
            @error('price')
                <div style="color: red; font-size:19px; margin-left:10px">
                  *{{$message}}
                </div>
            @enderror
          </div>
          <div class="form-group">
            <label>Description (optional)</label>
            <textarea name="description" style="height: 200px; resize:none" class="form-control" rows="3" placeholder="Masukan deskripsi disini"></textarea>
            @error('description')
            <div style="color: red; font-size:19px; margin-left:10px">
              *{{$message}}
            </div>
        @enderror
          </div>
        <div class="form-group">
            <label>Kategori Produk</label>
            <select class="form-control select2" name="category" id="category"  style="width: 100%;">
              @foreach ($data as $category)
                 <option value={{$category->id}}>{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
        <div class="form-group">
          <div><img src="" style="width:300px" id='currentImg'/></div>
          <label for="exampleInputFile">Masukan gambar produk</label>
          <div class="input-group">
            <div class="custom-file">
              <input accept="image/*" onchange="document.getElementById('currentImg').src = window.URL.createObjectURL(this.files[0])" type="file" name="image" >
            </div>
          </div>
          @error('image')
          <div style="color: red; font-size:19px; margin-left:10px">
            *{{$message}}
          </div>
      @enderror
        </div>
      </div>

      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Buat</button>
      </div>
    </form>
  </div>
@endsection