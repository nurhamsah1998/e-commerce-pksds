@extends('admin.components.drawer')

@section('content')
<div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Daftar Category</h1>
      </div>
    </div>
    <div style="display: flex; justify-content:flex-end; margin-bottom:20px">
      <a href="/admin/category/create-category" style="
        padding:5px;
        background-color:aquamarine;
        cursor:pointer;
        border-radius:10px

        ">Buat Category</a>
    </div>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Nama Kategori</th>
          <th scope="col" ></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($category as $item )
        <tr>
          <td >{{$item->name}}</td>
          <td>
             <div style="display: flex; align-items:center; gap:10px; justify-content:flex-end">
              <a href="/admin/category/{{$item->id}}" class="btn btn-info">Show</a>
              <a href="/admin/category/{{$item->id}}" class="btn btn-primary">Edit</a>
              <section class="content">
                <div class="modal fade" id="modal-delete">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Konfirmasi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>Apakah Anda Yakin Ingin menghapus {{$item->name}}?. Ingat data yang telah dihapus maka, ya dihapus. yakin ingin menghapus. gak bahaya tah ?</p>
                      </div>
                      <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <form action="/admin/category/{{$item->id}}" method="POST">
                          @csrf
                          @method('DELETE')
                        <button  type="submit" class="btn btn-primary">
                                      <a class=" text-light">
                                      Yup Saya Yakin
                                   </a>  
                      </button>
                    </form>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              </section>

                  <button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">
                    Hapus
                    </button>
             
             </div>
          </td>
      </tr>
      @empty
      <tr>
        <td colspan="6" style="text-align: center">
        Tidak ada produk yang ditampilkan.
        </td>
      </tr>  
        @endforelse
      </tbody>
  </table>

  </div>
@endsection