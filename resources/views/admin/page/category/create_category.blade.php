@extends('admin.components.drawer')

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Category Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/admin/category" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama Category</label>
          <input type="text" name="name" id="name" class="form-control" placeholder="Masukan nama category">
          @error('name')
              <div style="color: red; font-size:19px; margin-left:10px">
                *{{$message}}
              </div>
          @enderror
        </div>
      </div>

      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Buat</button>
      </div>
    </form>
  </div>
@endsection