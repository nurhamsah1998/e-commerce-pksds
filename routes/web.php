<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CartController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ADMIN
Route::get('/admin', [AdminController::class,'index']);
Route::get('/admin/category', [CategoryController::class,'index']);
Route::get('/admin/product-api', [ProductController::class,'index']);
Route::post('/admin/product', [ProductController::class,'store']);
Route::get('/admin/product/create-product', [ProductController::class,'create']);
Route::get('/admin/category/create-category', [CategoryController::class,'create']);
Route::post('/admin/category', [CategoryController::class,'store']);
Route::delete('/admin/product/{product_id}', [ProductController::class,'destroy']);
Route::delete('/admin/category/{product_id}', [CategoryController::class,'destroy']);
Route::put('/admin/product/{product_id}', [ProductController::class,'update']);
Route::put('/admin/category/{category_id}', [CategoryController::class,'update']);
Route::get('/admin/product/{product_id}', [ProductController::class,'edit']);
Route::get('/admin/category/{category_id}', [CategoryController::class,'edit']);
Route::get('/admin/product', [AdminController::class,'list_product']);
Route::get('/admin/category', [CategoryController::class,'index']);
Route::get('/admin/setting', [AdminController::class,'setting']);

Auth::routes();

Route::get('/', [ProductController::class, 'index_product']);
Route::get('/product/{product}', [ProductController::class, 'show_product']);



//USER LOGIN
Route::middleware('auth')->group(function() {
    Route::post('/cart/{product}', [CartController::class, 'add_to_cart']);
    Route::get('/cart', [CartController::class, 'show_cart']);
    Route::patch('/cart/{cart}', [CartController::class, 'update_cart']);
    Route::delete('/cart/{cart}', [CartController::class, 'delete_cart']);
    Route::post('/checkout', [OrderController::class, 'checkout']);
    Route::get('/order', [OrderController::class, 'index_order']);
    Route::get('/order/{order}', [OrderController::class, 'show_order']);
    Route::post('/order/{order}/pay', [OrderController::class, 'submit_payment_receipt']);
});