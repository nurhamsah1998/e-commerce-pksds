<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::with('category')->get();
        return ["produvt"=>$product];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Category::where('user_id', auth()->user()->id)->get();
        return view('admin.page.createProduct', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=> 'required',
            'price'=> 'required',
            'stock'=> 'required',
            'description'=> 'required',
            'image'=> 'required|mimes:png,jpg',
        ],[
            'image.required' => "Gambar wajib diisi ya ges ya!!",
            'image.mimes' => "Gambar yang didukung hanya PNG dan JPG"
        ]);
        
        $file_img = $request->file('image');
        $file_extension = $file_img->extension();
        $new_name_img = date('ymdhis')."." . $file_extension;
        $file_img->move(public_path('product_img'), $new_name_img);



        /// https://stackoverflow.com/a/8529678/18038473
        $idCategory = intval($request->category);
        Product::create([
            "name"=> $request->input('name'),
            "stock"=> $request->input('stock'),
            "price"=> $request->input('price'),
            "description"=> $request->input('description'),
            "category_id"=> $idCategory,
            "image"=> $new_name_img,
        ]);
            toastr()->success('Berhasil menambah produk.',['timeOut' => 5000]);
             return redirect('/admin/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::all();
        return view('admin.page.editProduct', compact(['product','category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $this->validate($request,[
            'name'=> 'required',
            'price'=> 'required',
            'stock'=> 'required',
            'description'=> 'required',
        ]);
        $new_name_img = $product->image;
        if($request->hasFile('image')){
            File::delete(public_path('product_img').'/'.$product->image);
            $file_img = $request->file('image');
            $file_extension = $file_img->extension();
            $new_name_img = date('ymdhis')."." . $file_extension;
            $file_img->move(public_path('product_img'), $new_name_img);
        }




        /// https://stackoverflow.com/a/8529678/18038473
        $idCategory = intval($request->category);
        $product->name = $request->name;
        $product->stock = $request->stock;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->category_id = $idCategory;
        $product->image = $new_name_img;
        $product->update();
            toastr()->success('Berhasil mengupdate produk.',['timeOut' => 5000]);
             return redirect('/admin/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $product = Product::find($id);
      File::delete(public_path('product_img').'/'.$product->image);
      $product->delete();
      toastr()->success('Berhasil menghapus.'. $product->name,['timeOut' => 5000]);
      return redirect('/admin/product');
    }

    //USER
    public function index_product()
    {
        $user = Auth::user();

        /// JIKA YNG LOGIN ADMIN LREDIRECT KE TAMPILAN ADMIN
        $isNull = is_null(auth()->user()) ? "user": $user->role;
        if($isNull !== "user"){
            return redirect('admin');
        }
        /// 
        $products = Product::all();
        return view('index_product', [
            'products' => $products,
            'user' => $user,
        ]);
    }

    public function show_product($id)
    {
        $product = Product::find($id);
        return view('show_product', compact('product'));
    }
}
