<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'IsAdmin']);
    }

    public function index()
    {
        return view('admin.page.dashboard');
    }
    public function list_product()
    {
        $product = Product::with('category')->get();
        return view('admin.page.produk', compact('product'));
    }
    public function setting()
    {
        return view('admin.page.pengaturan');
    }
}
