<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::with('product')->where('user_id', auth()->user()->id)->get();
        return view('admin.page.category.index_category',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.category.create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=> 'required',
        ],[
            "name.required"=> "Ayoyo tidak boleh kosong lah !!!"
        ]);
                Category::create([
                    "name"=> $request->input('name'),
                    "user_id"=> auth()->user()->id,
                ]);
                    toastr()->success('Berhasil menambah category.',['timeOut' => 5000]);
                     return redirect('/admin/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.page.category.update_category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $this->validate($request,[
            'name'=> 'required',
        ],[
            "name.required"=> "Ayoyo tidak boleh kosong lah !!!"
        ]);
        $category->name = $request->name;
        $category->update();
            toastr()->success('Berhasil mengupdate category.',['timeOut' => 5000]);
             return redirect('/admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        toastr()->success('Berhasil menghapus.'. $category->name,['timeOut' => 5000]);
        return redirect('/admin/category');
    }
}
